<?php
    include("functions.php");
    if($_POST['new'] == "1") {
        $registerAttempt = AttemptRegister($_POST);

        if($registerAttempt['success']) {
            $_SESSION['userId'] = $registerAttempt['id'];
            header('Location: index.php');
        }
        elseif($registerAttempt['msg']) {
            $loginError = $registerAttempt['msg'];
        }
        else {
            $loginError = 'Sorry, something went wrong; your account was not created.';
        }
    }
    elseif($_POST['new'] === "0") {
        $loginAttempt = AttemptLogin($_POST);
        if($loginAttempt['success']) {
            $_SESSION['userId'] = $loginAttempt['id'];
            header('Location: index.php');
        }

        $loginError = "Sorry, we could not validate your credentials.";
    }

    if($_GET['logout']) {
        unset($_SESSION['userId']);
    }
?>