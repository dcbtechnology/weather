$(document).ready(function() {

    // if the user is creating an account, we'll want to add some POST data before submitting the form
    $('#registerBtn').click(function(e) {
        e.preventDefault();
        $('#loginForm').append('<input type="hidden" name="new" value="1" />');
        $('#loginForm').submit();
    });

});