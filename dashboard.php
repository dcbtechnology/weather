<?php
    include("functions.php");

    // If the user attempted to add a location
    if($_POST['location']) {
        $jsonResponse = file_get_contents("https://api.openweathermap.org/data/2.5/weather?q=" . urlencode($_POST['location']) . "&appid=" . $apiKey);
        $weather = json_decode($jsonResponse);

        if($weather && $weather->cod == '200') {
            $success = AddLocationForUser($weather->id,$weather->name,$_SESSION['userId']);
            if($success) {
                $addLocationSuccess = "A new location has been added!";
            }
            else {
                $addLocationFail = "Something went wrong while saving your location.";
            }
        }
        else {
            $addLocationFail = "We did not recognize your location.";
        }
    }

    // Check if user is logged in
    if($_SESSION['userId']) {
        $user = GetUserDataById($_SESSION['userId']);
        $locations = GetLocationsByUserId($user['id']);
    }

    // if user is not logged in, redirect to login/registration
    if(!$user) {
        header('Location: login.php');
    }

    // We're done, close the connection to the DB
    CloseDbConnection();
?>