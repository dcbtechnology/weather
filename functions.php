<?php
    session_start();

    // Get database connection for querying 
    $db = SetUpDb();
    $apiKey = '75119b8855caaf439c5708b4dcec1a3d';

    if(!$db) {
        echo "Error connecting to database. Go troubleshoot." . PHP_EOL;
        exit;
    }

    /* Functions start here */
    function SetUpDb()
    {
        $db = mysqli_connect("127.0.0.1", "dcb_weather", "IAmWeather", "dcb_weather");

        if (!$db) {   
            return false;
        }

        return $db;
    }

    function CloseDbConnection()
    {
        global $db;
        $db->close();
    }

    function GetUserDataById($id)
    {
        // Select information about a user from an ID.
        global $db;
        $query = "SELECT * FROM user WHERE id=?";
        if ($stmt = $db->prepare($query)) {
            $stmt->bind_param("i",$id);
            $stmt->execute();
            $result = $stmt->get_result();

            if(!$result || $result->num_rows === 0) {
                return null;
            }

            $resultArray = array();
            while($row = $result->fetch_assoc()) {
                $resultArray = $row;
            }

            $stmt->close();
            $resultArray['success'] = true;
            return $resultArray;
        }

        return false;
    }

    function AttemptLogin($data)
    {
        global $db;
        $query = "SELECT * FROM user WHERE username=? AND password=?";
        if ($stmt = $db->prepare($query)) {
            $stmt->bind_param("ss",$_POST['username'],md5($_POST['password']));
            $stmt->execute();
            $result = $stmt->get_result();

            if(!$result || $result->num_rows === 0) {
                return array('success' => false, 'msg' => 'Invalid login.');
            }

            $resultArray = array();
            while($row = $result->fetch_assoc()) {
                $resultArray = $row;
            }

            $stmt->close();
            $resultArray['success'] = true;
            return $resultArray;
        }

        return array('success' => false, 'msg' => 'Invalid login.');
    }
    
    function AttemptRegister($data)
    {
        // Check if the username already exists
        global $db;
        $query = "SELECT * FROM user WHERE username=? AND password=?";
        if ($stmt = $db->prepare($query)) {
            $stmt->bind_param("ss",$_POST['username'],md5($_POST['password']));
            $stmt->execute();
            $result = $stmt->get_result();

            if($result && $result->num_rows > 0) {
                return array('success' => false, 'msg' => 'Your input was invalid (the user may already exist).');
            }

            $stmt->close();
        }

        $query = "INSERT INTO user (username, password) VALUES (?, ?)";
        if ($stmt = $db->prepare($query)) {
            $stmt->bind_param("ss", $data['username'], md5($data['password']));
            $stmt->execute();
            $result = $stmt->get_result();
            $newId = $db->insert_id;
            $stmt->close();

            return array('success' => true, 'id' => $newId);
        }

        return array('success' => false, 'msg' => 'Something went wrong while creating your account.');
    }

    function GetLocationsByUserId($userId)
    {
        global $db;
        $query = "SELECT * FROM location WHERE user_id=?";
        if ($stmt = $db->prepare($query)) {
            $stmt->bind_param("i",$userId);
            $stmt->execute();
            $result = $stmt->get_result();

            $resultArray = array();
            while($row = $result->fetch_assoc()) {
                $resultArray[] = $row;
            }

            $stmt->close();
            return $resultArray;
        }

        return false;
    }

    function AddLocationForUser($locationId,$locationName,$userId)
    {
        global $db;
        $query = "INSERT INTO location (api_id, name, user_id) VALUES (?, ?, ?)";
        if ($stmt = $db->prepare($query)) {
            $stmt->bind_param("isi", $locationId, $locationName, $userId);
            $stmt->execute();
            $result = $stmt->get_result();
            $stmt->close();

            return true;
        }

        return false; // Returns false if something went wrong
    }

    function GetWeatherForLocationById($id)
    {
        global $apiKey;
        $jsonResponse = file_get_contents("https://api.openweathermap.org/data/2.5/weather?id=" . $id . "&appid=" . $apiKey);
        $weather = json_decode($jsonResponse);
        return $weather;
    }

    function TempConvert($k)
    {
        return round(($k - 273.15) * 9/5 + 32);
    }

?>